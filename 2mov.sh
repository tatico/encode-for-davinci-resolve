#!/bin/bash

file=`basename $1`
ffmpeg -i "$1" -c:v mpeg4 -q:v 5 -c:a pcm_s16le "${file%.*}.mov"